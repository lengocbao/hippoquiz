package com.example.win7.hippoquiz;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

public class WelcomeActivity extends AppCompatActivity {
    LinearLayout welcomeScreen;
    LinearLayout ltC;
    LinearLayout ctdl;
    LinearLayout mdt;
    LinearLayout ltAndroid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        welcomeScreen = (LinearLayout) findViewById(R.id.welcome);
        ltC = (LinearLayout) findViewById(R.id.ltC);
        ctdl = (LinearLayout) findViewById(R.id.ctdl);
        mdt = (LinearLayout) findViewById(R.id.mdt);
        ltAndroid = (LinearLayout) findViewById(R.id.ltandroid);
        ctdl.setVisibility(View.GONE);
        ltC.setVisibility(View.GONE);
        mdt.setVisibility(View.GONE);
        ltAndroid.setVisibility(View.GONE);
// Animation khi vao trang
        final Animation animationWelcome = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.fadein);
        welcomeScreen.startAnimation(animationWelcome);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ctdl.setVisibility(View.VISIBLE);
                final Animation animation_ctdl = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slidein);
                ctdl.startAnimation(animation_ctdl);
            }
        }, 100);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ltC.setVisibility(View.VISIBLE);
                final Animation animation_ltC = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slidein);
                ltC.startAnimation(animation_ltC);

            }
        }, 200);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mdt.setVisibility(View.VISIBLE);
                final Animation animation_mdt = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slidein);
                mdt.startAnimation(animation_mdt);
            }
        }, 300);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ltAndroid.setVisibility(View.VISIBLE);
                final Animation animation_Android = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slidein);
                ltAndroid.startAnimation(animation_Android);
            }
        }, 400);
// ket thuc Animation khi vao trang

        // Animation khi tap vao cac mon hoc
        ctdl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //... Intent....
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_Android_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ltC.startAnimation(animation_Android_out);
                    }
                }, 100);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_ltc_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        mdt.startAnimation(animation_ltc_out);

                    }
                }, 200);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_mdt_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ltAndroid.startAnimation(animation_mdt_out);
                        ltC.setVisibility(View.INVISIBLE);
                        ltAndroid.setVisibility(View.INVISIBLE);
                        mdt.setVisibility(View.INVISIBLE);
                    }
                }, 300);
            }
        });


        ltC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //... Intent....
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_Android_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ctdl.startAnimation(animation_Android_out);
                    }
                }, 100);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_ltc_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        mdt.startAnimation(animation_ltc_out);

                    }
                }, 200);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_mdt_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ltAndroid.startAnimation(animation_mdt_out);
                        ctdl.setVisibility(View.INVISIBLE);
                        ltAndroid.setVisibility(View.INVISIBLE);
                        mdt.setVisibility(View.INVISIBLE);
                    }
                }, 300);
            }
        });

        mdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //... Intent....
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_Android_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ctdl.startAnimation(animation_Android_out);
                    }
                }, 100);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_ltc_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ltC.startAnimation(animation_ltc_out);

                    }
                }, 200);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_mdt_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ltAndroid.startAnimation(animation_mdt_out);
                        ctdl.setVisibility(View.INVISIBLE);
                        ltAndroid.setVisibility(View.INVISIBLE);
                        ltC.setVisibility(View.INVISIBLE);
                    }
                }, 300);
            }
        });

        ltAndroid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //... Intent....
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_Android_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ctdl.startAnimation(animation_Android_out);
                    }
                }, 100);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_ltc_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        ltC.startAnimation(animation_ltc_out);

                    }
                }, 200);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Animation animation_mdt_out = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.slideout);
                        mdt.startAnimation(animation_mdt_out);
                        ctdl.setVisibility(View.INVISIBLE);
                        ltC.setVisibility(View.INVISIBLE);
                        mdt.setVisibility(View.INVISIBLE);
                    }
                }, 300);
            }
        });
    }
}
