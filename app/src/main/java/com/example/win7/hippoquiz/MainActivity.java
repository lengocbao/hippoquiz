package com.example.win7.hippoquiz;

import android.content.Intent;

import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ProgressBar spinner;
    EditText username;
    EditText password;
    Button login;
    String defaultUsername="lengocbao";
    String defaultPassword="12345678";
    TextView failedLogin;
    Database database;
    TextView demo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        username=(EditText) findViewById(R.id.username);
        password=(EditText)findViewById(R.id.password);
        login=(Button)findViewById(R.id.login);
        failedLogin=(TextView)findViewById(R.id.failedLogin);
        failedLogin.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);
        database=new Database(this,"Student.sqlite",null,1);

//        database.QueryData("CREATE TABLE User(mssv INTEGER PRIMARY KEY,username VARCHAR(50),password VARCHAR(50),fullname VARCHAR(50),diemCTDL FLOAT,diemLTC FLOAT, diemMDT FLOAT, diemLTA FLOAT ) ");
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usernameText=username.getText().toString();
                String passwordText=password.getText().toString();
                String sql="SELECT *FROM User WHERE username=" + "'" +usernameText+ "'" +"AND password=" + "'" + passwordText + "'";
                Cursor dataQuery=   database.GetData(sql);
                while(dataQuery.moveToNext()){
                    spinner.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intentWelcome=new Intent(MainActivity.this,WelcomeActivity.class);
                            startActivity(intentWelcome);
                            spinner.setVisibility(View.GONE);
                            failedLogin.setVisibility(View.GONE);
                        }
                    },3000);
                }
                if(!dataQuery.moveToNext()){
                    spinner.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            failedLogin.setVisibility(View.VISIBLE);
                            spinner.setVisibility(View.GONE);
                        }
                    },3000);
                }
            }
        });
    }
}
